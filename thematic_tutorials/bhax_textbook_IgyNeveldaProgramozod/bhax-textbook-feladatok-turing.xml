<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Turing!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Végtelen ciklus</title>
        <para>
            Írj olyan C végtelen ciklusokat, amelyek 0 illetve 100 százalékban dolgoztatnak egy magot és egy olyat, amely  
            100 százalékban minden magot!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/JdYwKb-VdMo">https://youtu.be/JdYwKb-VdMo</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/infty-f.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/infty-f.c</filename>
            </link>, 
            <link xlink:href="Turing/infty-w.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/infty-w.c</filename>
            </link>.
        </para>
        <para>
            Végtelen ciklus több féle képpen lehet létrehozni.
            Ennek több oka is lehet például hogy egy szeverfolyamat fusson.
            A mi feladatunkban a pagerank algoritmust használjuk.
            
        </para>                    
        <para>
            Processzorhasználat 100 százalék, egy vegtelen ciklussal.
            Egy mag 100 százalékban:               
        </para>
        <programlisting language="c"><![CDATA[int
main ()
{
  for (;;);

  return 0;
}
]]>
        </programlisting>        
        <para>        
        vagy az olvashatóbb, de a programozók és fordítók (szabványok) között kevésbé hordozható
        </para>
        <programlisting language="c"><![CDATA[int
#include <stdbool.h>
main ()
{
  while(true);

  return 0;
}
]]>
        </programlisting>        
        <para>
            Azért érdemes a <literal>for(;;)</literal> hagyományos formát használni, 
            mert ez minden C szabvánnyal lefordul, másrészt
            a többi programozó azonnal látja, hogy az a végtelen ciklus szándékunk szerint végtelen és nem szoftverhiba. 
            Mert ugye, ha a <literal>while</literal>-al trükközünk egy nem triviális 
            <literal>1</literal> vagy <literal>true</literal> feltétellel, akkor ott egy másik, a forrást
            olvasó programozó nem látja azonnal a szándékunkat.
        </para>            
        <para>
            Egyébként a fordító a <literal>for</literal>-os és 
            <literal>while</literal>-os ciklusból ugyanazt az assembly kódot fordítja:
        </para>            
        <screen><![CDATA[$ gcc -S -o infty-f.S infty-f.c 
$ gcc -S -o infty-w.S infty-w.c 
$ diff infty-w.S infty-f.S 
1c1
< 	.file	"infty-w.c"
---
> 	.file	"infty-f.c"
]]></screen>  
        <para>
            Egy mag 0 százalékban:               
        </para>        
        <programlisting language="c"><![CDATA[#include <unistd.h>
int
main ()
{
  for (;;)
    sleep(1);
    
  return 0;
}
]]>
        </programlisting>        
        <para>
            Minden mag 100 százalékban:               
        </para>

        <programlisting language="c"><![CDATA[#include <omp.h>
int
main ()
{
#pragma omp parallel
{
  for (;;);
}
  return 0;
}
]]>
        </programlisting>        
        <para>
            A <command>gcc infty-f.c -o infty-f -fopenmp</command> parancssorral készítve a futtathatót, majd futtatva,               
            közben egy másik terminálban a <command>top</command> parancsot kiadva tanulmányozzuk, mennyi CPU-t használunk:            
        </para>
        <screen><![CDATA[top - 20:09:06 up  3:35,  1 user,  load average: 5,68, 2,91, 1,38
Tasks: 329 total,   2 running, 256 sleeping,   0 stopped,   1 zombie
%Cpu0 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu1 : 99,7 us, 0,3 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu2 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu3 : 99,7 us, 0,3 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu4 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu5 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu6 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu7 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
KiB Mem :16373532 total,11701240 free, 2254256 used, 2418036 buff/cache
KiB Swap:16724988 total,16724988 free,       0 used. 13751608 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND     
 5850 batfai    20   0   68360    932    836 R 798,3  0,0   8:14.23 infty-f     
]]></screen>  
                                
        <tip>
            <title>Werkfilm</title>
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/lvmi6tyz-nI">https://youtu.be/lvmi6tyz-nI</link>
                    </para>    
                </listitem>                
            </itemizedlist>                
        </tip>
    </section>        
        
    <section>
        <title>Lefagyott, nem fagyott, akkor most mi van?</title>
        <para>
            Mutasd meg, hogy nem lehet olyan programot írni, amely bármely más programról eldönti, hogy le fog-e fagyni vagy sem!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása:  tegyük fel, hogy akkora haxorok vagyunk, hogy meg tudjuk írni a <function>Lefagy</function>
            függvényt, amely tetszőleges programról el tudja dönteni, hogy van-e benne vlgtelen ciklus:              
        </para>
        <programlisting language="c"><![CDATA[Program T100
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	main(Input Q)
	{
		Lefagy(Q)
	}
}]]></programlisting>            
        <para>
            A program futtatása, például akár az előző <filename>v.c</filename> ilyen pszeudókódjára:
            <screen><![CDATA[T100(t.c.pseudo)
true]]></screen>            
            akár önmagára
            <screen><![CDATA[T100(T100)
false]]></screen>  
            ezt a kimenetet adja.          
        </para>
        <para>
            A T100-as programot felhasználva készítsük most el az alábbi T1000-set, amelyben a
            Lefagy-ra épőlő Lefagy2 már nem tartalmaz feltételezett, csak csak konkrét kódot:
        </para>
        <programlisting language="c"><![CDATA[Program T1000
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	boolean Lefagy2(Program P)
	{
		 if(Lefagy(P))
			return true;
		 else
			for(;;); 
	}

	main(Input Q)
	{
		Lefagy2(Q)
	}

}]]></programlisting>            
        <programlisting><![CDATA[]]></programlisting>            
        <para>
            Mit for kiírni erre a <computeroutput>T1000(T1000)</computeroutput> futtatásra?
                                
            <itemizedlist>
                <listitem>
                    <para>Ha T1000 lefagyó, akkor nem fog lefagyni, kiírja, hogy true</para>                        
                </listitem>
                <listitem>
                    <para>Ha T1000 nem fagyó, akkor pedig le fog fagyni...</para>                        
                </listitem>
            </itemizedlist>
            akkor most hogy fog működni? Sehogy, mert ilyen <function>Lefagy</function>
            függvényt, azaz a T100 program nem is létezik.                
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat:
		    Alan Turing, a probléma vizsgálása idején bizonyította, egy
            matematika bizonyítás segítségével, hogy ilyen programot nem lehet
            létre hozni, míg esetleg kisebb programnál szemre meg lehet állapítani, 
            de egy bonyolultabbnál kiszámíthatatlan... 
        </para>
    </section>        
                
    <section>
        <title>Változók értékének felcserélése</title>
        <para>
            Írj olyan C programot, amely felcseréli két változó értékét, bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül!
	
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/Ir8beXNtde4">https://youtu.be/lvmi6tyz-nI</link>
        </para>
        <para>
            Megoldás forrása:<programlisting language="c"><![CDATA[ 
#include <stdio.h>
int main()
{
	int a =7;
	int b =5;
	printf("a=%d b=%d\n", a, b);

	b = b - a;
	a = a + b;
 	b = a - b;
	printf("a=%d b=%d\n", a, b);
	
} 
]]>
        </programlisting>

        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat:
		Dekralaltam ket valtozot segedvaltozo nelkul akartam megcserelni
		b bol kivontam a-t majd a hoz hozza adtam b-t majd a bolt kivontam b-t ez szorzas osztassal is ugyan igy mukodne +=*  -=/
        </para>
    </section>                     

    <section>
        <title>Labdapattogás</title>
        <para>
            Először if-ekkel, majd bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül írj egy olyan programot, ami egy labdát pattogtat a karakteres konzolon! (Hogy mit értek
            pattogtatás alatt, alább láthatod a videókon.)
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/bRH0j-duiuU">https://youtu.be/bRH0j-duiuU</link>
        </para>
        <para>
            Megoldás forrása:<programlisting language="c"><![CDATA[
#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int xnov = 2;
    int ynov = 2;

    int mx;
    int my;

    for ( ;; ) {
	getmaxyx (ablak, my,mx);
	mvprintw(y,x,"o");
	
	refresh();
	usleep(55000);
	
	clear();
	x = x + xnov;
	y = y  +ynov;
	if ( x>mx-1){
		xnov = xnov * -1;	
	}
	if (x<=0){
		xnov = xnov * -1;
	}
	if (y<=0){
		ynov = ynov * -1;
	}
	if (y>my-1){
		ynov = ynov * -1;	
	}  

    }

    return 0;
}
]]>
        </programlisting>
	<programlisting language="c++">
	<![CDATA[
	#include <ncurses.h>
	#include <unistd.h>
	#include <stdlib.h>
	int main()
	{
		int xi = 0, xk = 0, yj = 0, yk = 0, mx = 0, my = 0, h = 0, w =
	    	0;
		initscr();
		cbreak();
		noecho();
		nodelay(stdscr, TRUE);
		getmaxyx(stdscr, h, w);
		mx = w * 2;
		my = h * 2;
		while (true) {
			xi = (xi - 1) % mx;
			xk = (xk + 1) % mx;
			yi = (yi - 1) % my;
			yk = (yk + 1) % my;
			clear();
			mvprintw(abs((yi + (my - yk)) / 2),
				 abs((xi + (mx - xk)) / 2), "o");
			refresh();
			usleep(40000);
		}	
		return 0;
	}
]]>
        </programlisting>

        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat:
            A fő feladatunk ennél a problémánál, hogy olyan függvényt találjunk, 
            ami le írja a labda pattogását, de mégsem vesz fel olyan értéke, ko-ordinátát, 
            amely kivül esik a megjeleníthető kordinátákon, azaz a látható, 
            konzolon belül pattog a "labda".
            A lenyeg hogy ne vegyen fel olyan kordinátát, 
            amely kivül esik a megjeleníthető kordinátákon
	    A <code>ncurses</code> függvénykönyvtárat hívjuk 
            segítségül, mely a terminálos interfészek (TUI-k) létrehozására
            lett készítve.
        </para>
    </section>                     

    <section>
        <title>Szóhossz és a Linus Torvalds féle BogoMIPS</title>
        <para>
            Írj egy programot, ami megnézi, hogy hány bites a szó a gépeden, azaz mekkora az <type>int</type> mérete.
            Használd ugyanazt a while ciklus fejet, amit Linus Torvalds a BogoMIPS rutinjában! 
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/ApkXmGB7rmo">https://youtu.be/ApkXmGB7rmo</link>, 
            
        </para>
        <para>
            Megoldás forrása:<programlisting language="c"><![CDATA[               
#include <stdio.h>
int main()
{
	"unsigned long int a = 1, count = 0;
	do
		count++;
	while (a <<= 1);
	printf("Szohossz: %ld.\n", count);
	return 0;
	
	}
]]>
        </programlisting>
	

	
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat: 
            A <code language="c">int</code> típusu változót kell hasznalnunk
	    melynek 1-es értéket adunk,
            aztan shifteljuk balra ameddig csak lehet, majd megszamoljuk,
            hogy hany shifteles tortent es ez lessz a szohossz a gepen.
 
        </para>
    </section>                     

    <section>
        <title>Helló, Google!</title>
        <para>
            Írj olyan C programot, amely egy 4 honlapból álló hálózatra kiszámolja a négy lap Page-Rank 
            értékét!
        </para>
        <para>
            Megoldás videó1: <link xlink:href="https://youtu.be/qR5syiCkL40"></link>
		Megoldás videó2: <link xlink:href="https://youtu.be/zO3xFPwwj4A"></link>
        </para>
        <para>
            Megoldás forrása:<programlisting language="c"><![CDATA[#include <stdio.h>
#include <math.h>
#include <stdlib.h>
void kiir(double tomb[], int db);
double tavolsag(double pagerank[], double pagerank_temp[], int db);
int main(void)
{
	double L[4][4] = {
		{0.0, 0.0, 1.0 / 3.0, 0.0},
		{1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
		{0.0, 1.0 / 2.0, 0.0, 0.0},
		{0.0, 0.0, 1.0 / 3.0, 0.0}
	};
	double PR[4] = { 0.0, 0.0, 0.0, 0.0 };
	double PRv[4] =
	    { 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0 };
	for (;;) {
		for (int i = 0; i < 4; i++)
			PR[i] = PRv[i];
		for (int i = 0; i < 4; i++) {
			double tmp = 0.0;
			for (int j = 0; j < 4; j++) {
				tmp += L[i][j] * PR[j];
				PRv[i] = tmp;
			}
		}
		if (tavolsag(PR, PRv, 4) < 0.000001)
			break;
	}
	kiir(PR, 4);
	return 0;
}
void kiir(double tomb[], int db)
{
	for (int i = 0; i < db; i++)
		printf("PageRank [%d]: %lf\n", i, tomb[i]);
}
double tavolsag(double pagerank[], double pagerank_temp[], int db)
{
	double tav = 0.0;
	for (int i = 0; i < db; i++) {
		tav +=
		    (pagerank[i] - pagerank_temp[i]) * (pagerank[i] -
							pagerank_temp
							[i]);
	}
	return sqrt(tav);
}
]]>
        </programlisting>

        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat:
	    Az algoritmust a googlenel fejlesztette, Larry Page és 
            Sergey Brin, az algoritmus arra az elméletre épül, hogy egy oldal 
            minnél több oldalra mutat, annak az értéke annál nagyobb de azt is fegyelembe kell
	    venni hogy magara az itelt oldalra hany masik mutat

	    PageRank egy olyan algoritmus, amely hiperlinkekkel osszekotott dokumentumokhoz 			szamokat rendel a halozatban betoltott szerepe alapjan.
        </para>
    </section>
                                                                                                                                                                                                                                                                                                                                                        
    <section xml:id="bhax-textbook-feladatok-turing.MontyHall">
        <title>A Monty Hall probléma</title>
        <para>
            Írj R szimulációt a nty MoHall problémára!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/QyVs00cQa0Q">https://youtu.be/QyVs00cQa0Q</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R</link>
	<programlisting language="c"><![CDATA[
kiserletek_szama=25000
kiserlet = sample(1:3, kiserletek_szama, replace=T)
jatekos = sample(1:3, kiserletek_szama, replace=T)
musorvezeto=vector(length = kiserletek_szama)

for (i in 1:kiserletek_szama) {

    if(kiserlet[i]==jatekos[i]){
    
        mibol=setdiff(c(1,2,3), kiserlet[i])
    
    }else{
    
        mibol=setdiff(c(1,2,3), c(kiserlet[i], jatekos[i]))
    
    }

    musorvezeto[i] = mibol[sample(1:length(mibol),1)]

}

nemvaltoztatesnyer= which(kiserlet==jatekos)
valtoztat=vector(length = kiserletek_szama)

for (i in 1:kiserletek_szama) {

    holvalt = setdiff(c(1,2,3), c(musorvezeto[i], jatekos[i]))
    valtoztat[i] = holvalt[sample(1:length(holvalt),1)]
    
}

valtoztatesnyer = which(kiserlet==valtoztat)


sprintf("Kiserletek szama: %i", kiserletek_szama)
length(nemvaltoztatesnyer)
length(valtoztatesnyer)
length(nemvaltoztatesnyer)/length(valtoztatesnyer)
length(nemvaltoztatesnyer)+length(valtoztatesnyer)
]]>
        </programlisting>
	
        </para>
        <para>
	Tortenet:A Monty Hall-paradoxon egy valószínűségi paradoxon, ami az Amerikai Egyesült 		Államokban futott Let's Make a Deal (Kössünk üzletet) című televíziós vetélkedő utolsó 		feladatán alapul, nevét a vetélkedő műsorvezetőjéről, Monty Hallról kapta
	
         Tanulságok, tapasztalatok, magyarázat:
	Kiserlet(vektor) az a ismetlesek szama
	3 ablak kozul 1 moge tesznek nyeremenyt
		
        </para>
    </section>

    <section xml:id="Brun">
        <title>100 éves a Brun tétel</title>
        <para>
            Írj R szimulációt a Brun tétel demonstrálására!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/iQjyfzX0WLA">https://youtu.be/iQjyfzX0WLA</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R</link>
        </para>

        <para>
            A természetes számok építőelemei a prímszámok. Abban az értelemben, 
            hogy minden természetes szám előállítható prímszámok szorzataként.
            Például 12=2*2*3, vagy például 33=3*11.
        </para>
        <para>
            Prímszám az a természetes szám, amely csak önmagával és eggyel 
            osztható. Eukleidész görög matematikus már Krisztus előtt tudta, 
            hogy végtelen sok prímszám van, de ma sem tudja senki, hogy 
            végtelen sok ikerprím van-e. Két prím ikerprím, ha különbségük 2.
        </para>
        <para>
            Két egymást követő páratlan prím között a legkisebb távolság a 2, 
            a legnagyobb távolság viszont bármilyen nagy lehet! Ez utóbbit 
            könnyű bebizonyítani. Legyen n egy tetszőlegesen nagy szám. 
            Akkor szorozzuk össze n+1-ig a számokat, azaz számoljuk ki az 
            1*2*3*… *(n-1)*n*(n+1) szorzatot, aminek a neve (n+1) faktoriális, 
            jele (n+1)!.
        </para>
        <para>
            Majd vizsgáljuk meg az a sorozatot:
        </para>    
        <para>
            (n+1)!+2, (n+1)!+3,… , (n+1)!+n, (n+1)!+ (n+1) ez n db egymást követő azám, ezekre (a jól ismert
            bizonyítás szerint) rendre igaz, hogy            
        </para>    
        <itemizedlist>
            <listitem>
                <para>(n+1)!+2=1*2*3*… *(n-1)*n*(n+1)+2, azaz 2*valamennyi+2, 2 többszöröse, így ami osztható kettővel</para>
            </listitem>
            <listitem>
                <para>(n+1)!+3=1*2*3*… *(n-1)*n*(n+1)+3, azaz 3*valamennyi+3, ami osztható hárommal</para>
            </listitem>
            <listitem>
                <para>...</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n-1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n-1)*valamennyi+(n-1), ami osztható (n-1)-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+n=1*2*3*… *(n-1)*n*(n+1)+n, azaz n*valamennyi+n-, ami osztható n-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n+1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n+1)*valamennyi+(n+1), ami osztható (n+1)-el</para>
            </listitem>
        </itemizedlist>
        <para>
            tehát ebben a sorozatban egy prim nincs, akkor a (n+1)!+2-nél 
            kisebb első prim és a (n+1)!+ (n+1)-nél nagyobb első 
            prim között a távolság legalább n.            
        </para>    
        <para>
            Az ikerprímszám sejtés azzal foglalkozik, amikor a prímek közötti 
            távolság 2. Azt mondja, hogy az egymástól 2 távolságra lévő prímek
            végtelen sokan vannak.
        </para>    
        <para>
            A Brun tétel azt mondja, hogy az ikerprímszámok reciprokaiból képzett sor összege, azaz
            a (1/3+1/5)+ (1/5+1/7)+ (1/11+1/13)+... véges vagy végtelen sor konvergens, ami azt jelenti, hogy ezek
            a törtek összeadva egy határt adnak ki pontosan vagy azt át nem lépve növekednek, 
            ami határ számot B<subscript>2</subscript> Brun konstansnak neveznek. Tehát ez
            nem dönti el a több ezer éve nyitott kérdést, hogy az ikerprímszámok halmaza végtelen-e? 
            Hiszen ha véges sok van és ezek
            reciprokait összeadjuk, akkor ugyanúgy nem lépjük át a B<subscript>2</subscript> Brun konstans értékét, 
            mintha végtelen 
            sok lenne, de ezek már csak olyan csökkenő mértékben járulnának hozzá a végtelen sor összegéhez, 
            hogy így sem lépnék át a Brun konstans értékét.     
        </para>
        <para>
            Ebben a példában egy olyan programot készítettünk, amely közelíteni próbálja a Brun konstans értékét.
            A repó <link xlink:href="../../../bhax/attention_raising/Primek_R/stp.r">
                <filename>bhax/attention_raising/Primek_R/stp.r</filename>
            </link> mevű állománya kiszámolja az ikerprímeket, összegzi
            a reciprokaikat és vizualizálja a kapott részeredményt.
        </para>
        <programlisting language="R">
<![CDATA[#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

library(matlab)

stp <- function(x){

    primes = primes(x)
    diff = primes[2:length(primes)]-primes[1:length(primes)-1]
    idx = which(diff==2)
    t1primes = primes[idx]
    t2primes = primes[idx]+2
    rt1plust2 = 1/t1primes+1/t2primes
    return(sum(rt1plust2))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")
]]>
        </programlisting>        
        <para>
            Soronként értelemezzük ezt a programot:
        </para>                
        <programlisting language="R">
<![CDATA[ primes = primes(13)]]>
        </programlisting>        
        <para>
            Kiszámolja a megadott számig a prímeket.             
        </para>
        <screen>
<![CDATA[> primes=primes(13)
> primes
[1]  2  3  5  7 11 13
]]>
        </screen>
                
        <programlisting language="R">
<![CDATA[ diff = primes[2:length(primes)]-primes[1:length(primes)-1]]]>
        </programlisting>        
        <screen>
<![CDATA[> diff = primes[2:length(primes)]-primes[1:length(primes)-1]
> diff
[1] 1 2 2 4 2
]]>
        </screen>        
        <para>
            Az egymást követő prímek különbségét képzi, tehát 3-2, 5-3, 7-5, 11-7, 13-11.
        </para>
        <programlisting language="R">
<![CDATA[idx = which(diff==2)]]>
        </programlisting>        
        <screen>
<![CDATA[> idx = which(diff==2)
> idx
[1] 2 3 5
]]>
        </screen>              
        <para>
            Megnézi a <varname>diff</varname>-ben, hogy melyiknél lett kettő az eredmény, mert azok az ikerprím párok, ahol ez igaz.
            Ez a <varname>diff</varname>-ben lévő 3-2, 5-3, 7-5, 11-7, 13-11 külünbségek közül ez a 2., 3. és 5. indexűre teljesül.
        </para>
        <programlisting language="R">
<![CDATA[t1primes = primes[idx]]]>
        </programlisting>  
        <para>
            Kivette a primes-ból a párok első tagját. 
        </para>
        <programlisting language="R">
<![CDATA[t2primes = primes[idx]+2]]>
        </programlisting>        
        <para>
            A párok második tagját az első tagok kettő hozzáadásával képezzük.
        </para>
        <programlisting language="R">
<![CDATA[rt1plust2 = 1/t1primes+1/t2primes]]>
        </programlisting>        
        <para>
            Az 1/t1primes a t1primes 3,5,11 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t1primes
[1] 0.33333333 0.20000000 0.09090909
]]>
        </screen>                      
        <para>
            Az 1/t2primes a t2primes 5,7,13 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t2primes
[1] 0.20000000 0.14285714 0.07692308
]]>
        </screen>                      
        <para>
            Az 1/t1primes + 1/t2primes pedig ezeket a törteket rendre összeadja.
        </para>
        <screen>
<![CDATA[> 1/t1primes+1/t2primes
[1] 0.5333333 0.3428571 0.1678322
]]>
        </screen>                      
        <para>
            Nincs más dolgunk, mint ezeket a törteket összeadni a 
            <function>sum</function> függvénnyel.
        </para>
        
        <programlisting language="R">
<![CDATA[sum(rt1plust2)]]>
        </programlisting>    
        <screen>
<![CDATA[>   sum(rt1plust2)
[1] 1.044023
]]>
        </screen>            
        <para>
            A következő ábra azt mutatja, hogy a szumma értéke, hogyan nő, egy határértékhez tart, a 
            B<subscript>2</subscript> Brun konstanshoz. Ezt ezzel a csipettel rajzoltuk ki, ahol először a fenti 
            számítást 13-ig végezzük, majd 10013, majd 20013-ig, egészen 990013-ig, azaz közel 1 millióig.
            Vegyük észre, hogy az ábra első köre, a 13 értékhez tartozó 1.044023.
        </para>
        <programlisting language="R">
<![CDATA[x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")]]>
        </programlisting>          
        <figure>
            <title>A B<subscript>2</subscript> konstans közelítése</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/BrunKorok.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A B<subscript>2</subscript> konstans közelítése</phrase>
                </textobject>
            </mediaobject>
        </figure>                             
        <tip>
            <title>Werkfilm</title>
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/VkMFrgBhN1g">https://youtu.be/VkMFrgBhN1g</link>
                    </para>    
                </listitem>                
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/aF4YK6mBwf4">https://youtu.be/aF4YK6mBwf4</link>
                    </para>    
                </listitem>                
            </itemizedlist>                
        </tip>
    </section>

</chapter>                
